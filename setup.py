#!/usr/bin/env python

from distutils.core import setup

setup(name='honey',
      version='dev.0',
#      description='Python Distribution Utilities',
      author='Maciej Wawrzynczuk',
      author_email='maciej.wawrzynczuk@gmail.com',
#      url='https://www.python.org/sigs/distutils-sig/',
      packages=['honey'],
      scripts=['reporter'],
      setup_requires=['pytest-runner'],
      tests_require=['pytest'],
     )
