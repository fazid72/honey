import pytest
from config_parser import ConfigParser


@pytest.fixture
def spam_parser():
    p = ConfigParser()
    p.add_symbol("spam", "eggs")
    return p
    
def test_parse_straight_value():
    p = ConfigParser()
    v = "straight_value"

    assert p.parse_value(v) == v


def test_parse_var(spam_parser):
    assert spam_parser.parse_value("@spam") == "eggs"


def test_parse_entry_simple(spam_parser):
    assert spam_parser("@spam") == "eggs"


def test_parse_list(spam_parser):
    c = ["@spam"]

    assert spam_parser(c) == ["eggs"]
    
def test_parse_nested_list(spam_parser):
    c = [["@spam"]]

    assert spam_parser(c) == [["eggs"]]

def test_parse_dict(spam_parser):
    c = {"becon": "@spam"}

    assert spam_parser(c) == {"becon": "eggs"}
