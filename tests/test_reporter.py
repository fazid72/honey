from reporter import mk_entry, mk_host_set, mk_report

def mk_echo_runner(str_to_add):
    def f(in_str):
        stdout = "{} {}".format(str_to_add, in_str)
        return {"stdout": stdout}
    
    return f


def test_mk_entry():
    entry_config = { "label": "Zupa",
                     "command": "silly_command" }

    run_echo = mk_echo_runner('echoing')
    
    result = mk_entry(run_echo, entry_config)

    assert result == {"Zupa": "echoing silly_command"}


def test_mk_host_set():
    hs_config = [{"label": "l1",
                  "command": "v1"},
                  {"label": "l2",
                   "command": "v2"}]
    runner = mk_echo_runner("r1")

    result = mk_host_set(runner, hs_config)

    assert result == {"l1": "r1 v1",
                      "l2": "r1 v2"}

def test_mk_report():
    config = [{"label": "l",
              "command": "v"}]
    runners = [mk_echo_runner("r1"),
               mk_echo_runner("r2")]

    result = mk_report(runners, config)

    assert result == [{"l": "r1 v"},
                      {"l": "r2 v"}]
