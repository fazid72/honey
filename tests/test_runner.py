from pytest import fixture
from runner import RunnerFactory


@fixture
def localhost():
    rf = RunnerFactory()
    return rf('localhost')

def test_true_runner(localhost):
    """true command should give exit_code 0"""

    command = 'true'

    result = localhost(command)

    assert result['exit_code'] == 0

def test_false_runner(localhost):
    """false command should give 1"""

    command = 'false'

    result = localhost(command)

    assert result['exit_code'] == 1

def test_stdout(localhost):
    """echo commands should give proper stdout"""

    test_string = "Zupa"
    command = "echo -n {}".format(test_string)

    result = localhost(command)

    assert result["stdout"] == test_string

def test_stderr(localhost):
    """should return proper stdout"""
    test_string = "Zupa"
    command = ">&2 echo -n {}".format(test_string)

    result = localhost(command)

    assert result['stderr'] == test_string
