import paramiko

class Runner:
    def __init__(self, client):
        self.client = client

    def __call__(self, command):
        stdint, stdout, stderr= self.client.exec_command(command)
        status = stdout.channel.recv_exit_status()
        out = stdout.read().decode("utf-8")
        err = stderr.read().decode("utf-8")
        return { "exit_code": status,
                 "stdout": out.strip(),
                 "stderr": err.strip()}

class RunnerFactory:    
    def __call__(self, host):
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(host)
        return Runner(client)
