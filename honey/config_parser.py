class ConfigParser:
    def __init__(self):
        self.symbols = {}

    def add_symbol(self, symbol, value):
        self.symbols[symbol] = value

    def parse_value(self, v):
        if v[0] == '@':
            return self.symbols[v[1:]]
        return v

    def parse_list(self, v):
        return map(self.__call__, v)

    def parse_dict(self, v):
        return {k: self.__call__(v[k]) for k in v}

    def __call__(self, v):
        if type(v) is list:
            return list(self.parse_list(v))
        elif type(v) is dict:
            return self.parse_dict(v)
        else:
            return self.parse_value(v)
