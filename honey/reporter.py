def mk_entry(runner, entry_config):
    result = runner(entry_config["command"])["stdout"]
    return {entry_config["label"]: result}


def mk_host_set(runner, configs):
    result = {}
    for c in configs:
        result.update(mk_entry(runner, c))

    return result

def mk_report(runners, configs):
    return [mk_host_set(r, configs) for r in runners]
