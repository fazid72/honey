VENV := venv
BIN := ${VENV}/bin
ACTIVATE_FILE := ${BIN}/activate

PYTHONPATH=honey
export PYTHONPATH

${ACTIVATE_FILE}: requirements.txt
	python3 -m virtualenv -p python3 ${VENV}
	${BIN}/pip install -r $<
	touch $@

venv: ${ACTIVATE_FILE}

test: venv
	${BIN}/pytest

sdist: venv
	${BIN}/python setup.py sdist

tc: venv
	${BIN}/pytest && git commit -am changed

clean:
	rm -rf ${VENV}

.PHONY: venv clean test tc
